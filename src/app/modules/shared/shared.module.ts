import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule, MatCheckboxModule, MatToolbarModule, MatMenuModule, MatIconModule, MatBadgeModule,
  MatSidenavModule,  MatListModule, MatFormFieldModule, MatInputModule} from '@angular/material';

  import {CommonService} from '../../services/common.service';
  import {ApiService} from '../../services/api.service';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    [MatButtonModule, MatCheckboxModule, MatToolbarModule, MatMenuModule, MatIconModule, MatBadgeModule,
    MatSidenavModule, MatListModule, MatFormFieldModule, MatInputModule]
  ],
  declarations: [],
  providers: [CommonService, ApiService],
  exports: [
    CommonModule,   
    ReactiveFormsModule,
    FlexLayoutModule,
    [MatButtonModule,
    MatCheckboxModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatBadgeModule,
    MatSidenavModule,
    MatListModule,
    MatFormFieldModule,
    MatInputModule,
   
    ]
  ]
})
export class SharedModule {}